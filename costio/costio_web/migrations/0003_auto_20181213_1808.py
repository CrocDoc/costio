# Generated by Django 2.1 on 2018-12-13 18:08

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('costio_web', '0002_auto_20181213_1804'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appuser',
            name='user',
        ),
        migrations.AlterField(
            model_name='expense',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='AppUser',
        ),
    ]
