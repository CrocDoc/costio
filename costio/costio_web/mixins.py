from django.contrib.auth.mixins import LoginRequiredMixin


class LoginRequiredNoNextMixin(LoginRequiredMixin):
    login_url = '/login'
    redirect_field_name = ''

