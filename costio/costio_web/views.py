from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic
from costio_web.models import Expense, Category
from django.contrib.auth import get_user_model, logout
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView
from . import forms
from .mixins import LoginRequiredNoNextMixin
from django.urls import reverse_lazy
from django.db.models import Sum


class IndexView(LoginRequiredNoNextMixin, generic.RedirectView):
    url = reverse_lazy('costio:expense_list')


class Login(LoginView):
    redirect_field_name = ''
    redirect_authenticated_user = True
    authentication_form = forms.LoginForm


class Logout(LogoutView):
    url = reverse_lazy('costio:login')

    def get_redirect_url(self, *args, **kwargs):
        logout(self.request)
        return super().get_redirect_url()


class Registration(generic.CreateView):
    form_class = forms.RegistrationForm
    template_name = 'registration/register.html'
    success_url = '/login'


class ExpenseList(LoginRequiredNoNextMixin, generic.ListView):
    model = Expense
    template_name = 'costio_web/expenses_list.html'

    def get_queryset(self):
        return Expense.objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['expense_list'] = Expense.objects.filter(
            user=self.request.user
        )
        context['expenses_data'] = Expense.objects.filter(
            user=self.request.user
        ).aggregate(expenses_sum=Sum('cost'))['expenses_sum']
        return context


class ExpenseCreate(LoginRequiredNoNextMixin, generic.CreateView):
    model = Expense
    form_class = forms.ExpenseForm
    template_name = 'costio_web/expense_create.html'

    def form_valid(self, form):
        expense = form.save(commit=False)
        expense.user = User.objects.get(id=self.request.user.id)
        expense.save()
        return HttpResponseRedirect(reverse_lazy('costio:expense_list'))


class ExpenseDelete(LoginRequiredNoNextMixin, generic.DeleteView):
    model = Expense
    template_name = 'costio_web/expense_delete.html'
    success_url = reverse_lazy('costio:expense_list')
