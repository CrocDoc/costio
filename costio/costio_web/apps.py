from django.apps import AppConfig


class CostioWebConfig(AppConfig):
    name = 'costio_web'
