from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from .models import Expense

class LoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ['username', 'password']

class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username']

class ExpenseForm(forms.ModelForm):
    class Meta:
        model = Expense
        exclude = ['date_of_creation', 'user']