from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.db import models
from django.db.models import fields
# Create your models here.


class Category(models.Model):
    name = fields.CharField(max_length=100)

    def __str__(self):
        return self.name


class Expense(models.Model):
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE
    )
    name = fields.CharField(default='Expense', max_length=50)
    cost = fields.IntegerField()
    date_of_creation = fields.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name






