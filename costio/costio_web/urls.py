from django.urls import path
from . import views

app_name = 'costio'
urlpatterns = [
    # User auth views
    path('', views.IndexView.as_view(), name='index'),
    path('login/', views.Login.as_view(), name='login'),
    path('register/', views.Registration.as_view(), name='registration'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('expenses/', views.ExpenseList.as_view(), name='expense_list'),
    path('expenses/create/', views.ExpenseCreate.as_view(), name='expense_create'),
    path('expenses/delete/<int:pk>', views.ExpenseDelete.as_view(), name='expense_delete')
]