from django.urls import path
from rest_framework.authtoken import views as authviews
from . import views

app_name = 'costio_api'
urlpatterns = [
    path('login/', authviews.obtain_auth_token),
    path('expenses/', views.ExpensesView.as_view())
]