from costio_web.models import Expense
from rest_framework import serializers


class ExpenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expense
        fields = ('name', 'cost', 'category')

    def create(self, validated_data):
        expense = Expense(**validated_data, user = self.context['request'].user)
        expense.save()
        return expense
