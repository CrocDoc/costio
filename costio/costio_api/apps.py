from django.apps import AppConfig


class CostioApiConfig(AppConfig):
    name = 'costio_api'
