from django.shortcuts import render
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from costio_web.models import Expense
from .serializers import ExpenseSerializer
# Create your views here.

class ExpensesView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ExpenseSerializer

    def get_queryset(self):
        return Expense.objects.filter(user=self.request.user)
